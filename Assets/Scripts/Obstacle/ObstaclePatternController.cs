﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstaclePatternController : MonoBehaviour
{
  public Transform characterTrs;
  public GameObject[] patterns;

  private const float PatternStartPosY = 6.4f;
  private const float PatternInterval = 12.8f;

  private Queue<GameObject> spawnedPatternQueue = new Queue<GameObject>();
  private int currPatternCount;

  private float CheckPatternPosY {
    get {
      return PatternInterval * currPatternCount;
    }
  }
  private float CurrPatternPosY {
    get {
      return PatternStartPosY + PatternInterval * currPatternCount;
    }
  }

  private bool isPlaying = false;


  public void StartPatternControl()
  {
    currPatternCount = 0;
    isPlaying = true;
  }

  public void StopPatternControl()
  {
    isPlaying = false;
  }

  void Update()
  {
    if (isPlaying == false)
      return;

    if (characterTrs.position.y >= CheckPatternPosY)
    {
      CreateRandomPattern();

      if (spawnedPatternQueue.Count >= 4)
        DeleteLastPattern();
    }
  }

  private void CreateRandomPattern()
  {
    var randomPatternPrefab = patterns[Random.Range(0, patterns.Length)];
    var randomPattern = Instantiate(randomPatternPrefab, transform);

    randomPattern.transform.position = new Vector2(0, CurrPatternPosY);
    spawnedPatternQueue.Enqueue(randomPattern);

    ++currPatternCount;
  }

  private void DeleteLastPattern()
  {
    if (spawnedPatternQueue.Count == 0)
      return;

    var lastPattern = spawnedPatternQueue.Dequeue();
    DestroyImmediate(lastPattern.gameObject);
  }

  public void ClearAllPattern()
  {
    foreach (var spawnedPattern in spawnedPatternQueue)
    {
      DestroyImmediate(spawnedPattern.gameObject);
    }
    spawnedPatternQueue.Clear();
  }
}
