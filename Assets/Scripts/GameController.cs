﻿using UnityEngine;

public class GameController : MonoBehaviour
{
  private static GameController _instance = null;
  public static GameController Instance {
    get {
      if (_instance != null)
        return _instance;

      _instance = FindObjectOfType<GameController>();

      if (_instance == null)
        Debug.LogError("싱글톤이 없습니다!");

      return _instance;
    }
  }

  public Character character;
  public ObstaclePatternController patternController;
  public GameUIController gameUIController;

  private int playerMeter;
  public int PlayerMeter {
    get { return playerMeter; }
    set {
      playerMeter = value;
      gameUIController.OnChangedGameMeter(value);
    }
  }

  void Start()
  {
    ShowTitle();
  }

  /// <summary>
  /// 게임 타이틀 보여주기
  /// </summary>
  public void ShowTitle()
  {
    patternController.ClearAllPattern();
    PlayerMeter = 0;
    character.InitForTitle();
    gameUIController.ShowTitle();
  }

  /// <summary>
  /// 게임이 시작할때
  /// </summary>
  public void OnStartGame()
  {
    character.InitForStart();
    patternController.StartPatternControl();
  }

  /// <summary>
  /// 게임이 종료될때
  /// </summary>
  public void OnGameOver()
  {
    character.Dead();
    patternController.StopPatternControl();
    gameUIController.ShowGameOverPopup(1f);
  }

  /// <summary>
  /// 게임이 재시작 될때
  /// </summary>
  public void OnResetGame()
  {
    ShowTitle();
  }
}