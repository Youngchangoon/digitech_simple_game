﻿using UnityEngine;

public enum CharacterState
{
  Living,
  Dead,
}

public class Character : MonoBehaviour
{
  public float jumpPower;
  public Vector2 jumpDir_L;
  public Vector2 jumpDir_R;
  public CharacterCamera characterCamera;

  private Rigidbody2D rigid2D;
  private CapsuleCollider2D charCollider;
  private CharacterState currState;

  void Awake()
  {
    rigid2D = GetComponent<Rigidbody2D>();
    charCollider = GetComponent<CapsuleCollider2D>();
    currState = CharacterState.Dead;
  }

  void Update()
  {
    if (currState == CharacterState.Dead)
      return;

    // Input 
    if (Input.GetKeyDown(KeyCode.LeftArrow))
    {
      rigid2D.velocity = Vector3.zero;
      rigid2D.AddForce(jumpDir_L * jumpPower, ForceMode2D.Impulse);
    }

    if (Input.GetKeyDown(KeyCode.RightArrow))
    {
      rigid2D.velocity = Vector3.zero;
      rigid2D.AddForce(jumpDir_R * jumpPower, ForceMode2D.Impulse);
    }

    GameController.Instance.PlayerMeter = (int)Mathf.Max(transform.position.y, GameController.Instance.PlayerMeter);
  }

  private void OnCollisionEnter2D(Collision2D coll2D)
  {
    if (currState == CharacterState.Dead)
      return;

    if (coll2D.collider.CompareTag("Obstacle"))
    {
      GameController.Instance.OnGameOver();
    }
  }

  public void InitForTitle()
  {
    ResetCharacter(true);
  }

  public void InitForStart()
  {
    if (currState == CharacterState.Dead)
    {
      ResetCharacter(false);
      return;
    }

    rigid2D.bodyType = RigidbodyType2D.Dynamic;
  }

  public void ResetCharacter(bool isKinematic)
  {
    currState = CharacterState.Living;
    gameObject.SetActive(true);

    transform.position = Vector2.zero;
    transform.rotation = Quaternion.identity;
    rigid2D.velocity = Vector2.zero;
    characterCamera.ResetPosition();

    charCollider.enabled = true;
    rigid2D.freezeRotation = true;

    rigid2D.bodyType = isKinematic ? RigidbodyType2D.Kinematic : RigidbodyType2D.Dynamic;
  }

  public void Dead()
  {
    currState = CharacterState.Dead;

    // ------
    // Dead Effect
    charCollider.enabled = false;

    rigid2D.velocity = Vector3.zero;
    rigid2D.freezeRotation = false;
    rigid2D.AddTorque(20f, ForceMode2D.Impulse);
  }
}
