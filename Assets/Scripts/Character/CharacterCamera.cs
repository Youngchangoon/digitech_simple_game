﻿using UnityEngine;

public class CharacterCamera : MonoBehaviour
{
  public Transform characterTransform;
  public float camOffsetY;
  private Transform camTransform;

  void Awake()
  {
    camTransform = GetComponent<Transform>();
  }

  void Update()
  {
    var camPos = camTransform.position;
    var charPos = characterTransform.position;

    if (charPos.y > camPos.y + camOffsetY)
    {
      camTransform.position = new Vector3(camPos.x, charPos.y - camOffsetY, camPos.z);
    }
  }

  public void ResetPosition()
  {
    camTransform.position = new Vector3(0, 0, camTransform.position.z);
  }
}
