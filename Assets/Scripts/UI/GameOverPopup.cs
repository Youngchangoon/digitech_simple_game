﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverPopup : MonoBehaviour
{
  public Text scoreText;
  public Text bestScoreText;

  private readonly string BestScoreSaveKey = "BestScore";

  public void ShowGameOverPopup()
  {
    var currScore = GameController.Instance.PlayerMeter;
    var bestScore = PlayerPrefs.GetInt(BestScoreSaveKey, 0);

    scoreText.text = currScore.ToString();
    bestScoreText.text = bestScore.ToString();

    CompareAndSaveScore(currScore, bestScore);

    gameObject.SetActive(true);
  }

  public void HideGameOverPopup()
  {
    gameObject.SetActive(false);
  }

  private void CompareAndSaveScore(int currScore, int compareScore)
  {
    if (currScore <= compareScore)
      return;

    PlayerPrefs.SetInt(BestScoreSaveKey, currScore);
  }

  public void OnPressedRestartButton()
  {
    HideGameOverPopup();
    GameController.Instance.OnResetGame();
  }
}
