﻿using UnityEngine;
using UnityEngine.UI;

public class GameUIController : MonoBehaviour 
{
  public TitleSection titleSection;
  public GameOverPopup gameOverPopup;
  public Text meterText;


  public void ShowTitle()
  {
    titleSection.ShowTitleSection();
  }

  /// <summary>
  /// Meter Text가 바뀔때
  /// </summary>
  /// <param name="newMeter"> 새로운 Meter </param>
  public void OnChangedGameMeter(int newMeter)
  {
    meterText.text = newMeter.ToString() + "<size=70>m</size>";
  }

  public void ShowGameOverPopup(float delay)
  {
    Invoke("DelayedShowGameOverPopup", delay);
  }

  void DelayedShowGameOverPopup()
  {
    gameOverPopup.ShowGameOverPopup();
  }

  public void HideGameOverPopup()
  {
    gameOverPopup.HideGameOverPopup();
  }
}