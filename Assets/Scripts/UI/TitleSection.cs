﻿using UnityEngine;

public class TitleSection : MonoBehaviour
{
  public void ShowTitleSection()
  {
    gameObject.SetActive(true);
  }

  public void HideTitleSection()
  {
    gameObject.SetActive(false);
  }

  void Update()
  {
    if (Input.anyKey)
    {
      HideTitleSection();
      GameController.Instance.OnStartGame();
    }
  }
}
